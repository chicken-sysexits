chicken-sysexits
================
`sysexits(3)` values (preferable exit codes) for CHICKEN.

API
---
After `(import (sysexits))`, the following integer values will be defined:

    exit/ok          ; successful termination
    exit/usage       ; command line usage error
    exit/dataerr     ; data format error
    exit/noinput     ; cannot open input
    exit/nouser      ; addressee unknown
    exit/nohost      ; host name unknown
    exit/unavailable ; service unavailable
    exit/software    ; internal software error
    exit/oserr       ; system error (e.g., can't fork)
    exit/osfile      ; critical OS file missing
    exit/cantcreat   ; can't create (user) output file
    exit/ioerr       ; input/output error
    exit/tempfail    ; temp failure; user is invited to retry
    exit/protocol    ; remote error in protocol
    exit/noperm      ; permission denied
    exit/config      ; configuration error

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
Public Domain
