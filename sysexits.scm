;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; sysexits(3) values (preferable exit codes) for CHICKEN.
;;;
;;; This software is written by Evan Hanson <evhan@foldling.org> and
;;; placed in the Public Domain. All warranties are disclaimed.
;;;

(module sysexits *
  (import (scheme) (chicken foreign))
  (foreign-declare "#include <sysexits.h>")
  (define exit/ok (foreign-value "EX_OK" int)) ; successful termination
  (define exit/usage (foreign-value "EX_USAGE" int)) ; command line usage error
  (define exit/dataerr (foreign-value "EX_DATAERR" int)) ; data format error
  (define exit/noinput (foreign-value "EX_NOINPUT" int)) ; cannot open input
  (define exit/nouser (foreign-value "EX_NOUSER" int)) ; addressee unknown
  (define exit/nohost (foreign-value "EX_NOHOST" int)) ; host name unknown
  (define exit/unavailable (foreign-value "EX_UNAVAILABLE" int)) ; service unavailable
  (define exit/software (foreign-value "EX_SOFTWARE" int)) ; internal software error
  (define exit/oserr (foreign-value "EX_OSERR" int)) ; system error (e.g., can't fork)
  (define exit/osfile (foreign-value "EX_OSFILE" int)) ; critical OS file missing
  (define exit/cantcreat (foreign-value "EX_CANTCREAT" int)) ; can't create (user) output file
  (define exit/ioerr (foreign-value "EX_IOERR" int)) ; input/output error
  (define exit/tempfail (foreign-value "EX_TEMPFAIL" int)) ; temp failure; user is invited to retry
  (define exit/protocol (foreign-value "EX_PROTOCOL" int)) ; remote error in protocol
  (define exit/noperm (foreign-value "EX_NOPERM" int)) ; permission denied
  (define exit/config (foreign-value "EX_CONFIG" int))) ; configuration error
